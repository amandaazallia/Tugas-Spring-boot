angular.module('aplikasiSpringboot')
        .factory('homeService', function ($http){
            return {
                getPeserta: function (){
                    return $http.get("/peserta");
                },
                getListPeserta: function (){
                    return $http.get("/ListPeserta");
                },
                getPesertaFromDb:function (data){
                    return $http.get("/api/peserta");
                },
                save: function (data) {
                    return $http.post("/api/peserta", data);
                },
                delete: function (id){
                    return $http.post("/api/peserta/"+id)
                },
                update:function (id, data){
                    return $http.put("/api/peserta/"+id,data);
                }
            };
});
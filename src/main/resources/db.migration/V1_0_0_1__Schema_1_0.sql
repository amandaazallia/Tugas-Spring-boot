DROP TABLE IF EXISTS `peserta`;
CREATE TABLE `peserta`(
`id` varchar(255) NOT NULL,
`nama` varchar(255) NOT NULL,
`alamat` varchar(255) NOT NULL,
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `c_security_role`(
`id` VARCHAR (255) NOT NULL,
`nama` VARCHAR (255) NOT NULL,
`keterangan` VARCHAR (255) NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `K_972999_i222_n12222_y1kk97` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `c_security_user`(
`id` VARCHAR (255) NOT NULL,
`username` VARCHAR (255) NOT NULL,
`password` VARCHAR (255) NOT NULL,
`active` BOOLEAN NOT NULL,
`id_role` VARCHAR (255) NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `K_972999_i222_n1288J_y1kk90` (`username`),
KEY `k_972999_i222_n1288J_y1kodt` (`id_role`),
CONSTRAINT `k_972999_i222_n1288J_y1kodt` FOREIGN KEY (`id_role`)
REFERENCES `c_security_role` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;
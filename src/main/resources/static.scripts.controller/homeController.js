angular.module('aplikasiSpringboot')
        .controller('homeController', function ($scope, homeService){
          $scope.hallo = 'Kiny';
          $scope.nama = 'Kiny';
          $scope.model = {};
          $scope.isEdit = false;
          
       
          
          $scope.data = [
              {"nama" : "amanda", "alamat" : "jakarta"},
              {"nama" : "kiny", "alamat" : "smea"}
          ]
          
          $scope.hapus = function (index){
              $scope.data.splice(index,1);
          }
          
          $scope.simpan = function(data){
              if($scope.isEdit == true){
                  $scope.data[$scope.indexEdit] = data;
              } else {
                $scope.data.push(data);
              }
              $scope.clear();
          }
          
          $scope.clear = function(){
              $scope.model = {};
              $scope.isEdit = false;
          }
          
          $scope.edit = function(data, index){
              $scope.model.nama = data.nama;
              $scope.model.alamat = data.alamat;
              $scope.indexEdit = index;
              $scope.isEdit = true;
          }
          
           $scope.tampilkanData = function(){
              homeService.getPesertaFromDb().success(function (data){
                  $scope.dataServer = data.content;
              });
          };
          $scope.isDataEdit = false;
          $scope.currentPeserta = {};
          $scope.simpanData = function (data) {
              if ($scope.isDataEdit == true) {
                  homeService.update(data.id, data),success(function (data) {
                      alert("data berhasil diubah");
                      $scope.clearData();
                      
                  }).error(function () {
                      alert("terjadi kesalahan dalam edit data");
                  })
              } else {
                  homeService.save(data).success(function (data) {
                      alert("data berhasil disimpan");
                      $scope.clearData();
                  }).error(function () {
                      alert("terjadi kesalahan dalam menyimpan data");
                  })
              }
          }      
          
          $scope.hapusData=function (id){
              homeService.delete(id).success(function (data){
                  alert("data berhasil di hapus");
                  $scope.clearData();
              }).error(function () {
                  alert("terjadi kesalahan dalam penghapusan data");
                  $scope.clearData();
              });
          };
          
          $scope.editData = function (data){
              $scope.isDataEdit = true;
              $scope.currentPeserta = {};
              $scope.currentPeserta.nama = data.nama;
              $scope.currentPeserta.alamat = data.alamat;
              $scope.currentPeserta.id = data.id;
          };
          $scope.clearData = function () {
              $scope.isDataEdit = false;
              $scope.currentPeserta = {};
              $scope.reloadData();
          }
          $scope.reloadData = function () {
              homeService.getPesertaFromDb().success(function (data) {
                  $scope.dataServer = data.content;
              })
          }
          
});
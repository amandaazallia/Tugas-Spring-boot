/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package azall.springboot.domain;

import azall.springboot.service.PesertaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author amanda
 */
@RestController
@RequestMapping("/api/peserta")
public class PesertaController {
    @Autowired
    private PesertaDao pesertaDao;
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Peserta finPesertaById(@PathVariable String id){
        return pesertaDao.findOne(id);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public void save(@RequestBody Peserta p){
        pesertaDao.save(p);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public Page<Peserta> findAll(Pageable pageable){
        return pesertaDao.findAll(pageable);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete (@PathVariable String id) {
        Peserta p = pesertaDao.findOne(id);
        if(p !=null){
            pesertaDao.delete(p);
        }
    }
    
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public void edit(@PathVariable String id, @RequestBody Peserta p){
        Peserta peserta = pesertaDao.findOne(id);
        if(peserta != null){
            p.setId(id);
            pesertaDao.save(p);
           
        }
    }
    
}

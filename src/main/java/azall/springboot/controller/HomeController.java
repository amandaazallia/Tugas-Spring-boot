/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package azall.springboot.controller;

import azall.springboot.domain.Peserta;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author amanda
 */
@RestController

public class HomeController {
    
    @RequestMapping("/hello")
    public String hello(){
        return "Hello World !";
    }
    
    @RequestMapping(value = 
            "/peserta",method = RequestMethod.GET)
    Peserta getPeserta() {
        Peserta p = new Peserta();
        p.setNama("Amanda");
        p.setAlamat("Jakarta");
        
        return p;
    }
    
    @RequestMapping(value = "/ListPeserta",method = RequestMethod.GET)
    List<Peserta> getListPeserta() {
        
        List<Peserta> pesertas = new ArrayList<Peserta>();
        Peserta p = new Peserta();
        p.setNama("Azallia");
        p.setAlamat("jlnusa");
        
        Peserta p1 = new Peserta();
        p1.setNama("kiny");
        p1.setAlamat("Jakarta");
        
        pesertas.add(p);
        pesertas.add(p1);
        
        return pesertas;
    }
}
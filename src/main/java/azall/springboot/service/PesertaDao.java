/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package azall.springboot.service;

import azall.springboot.domain.Peserta;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author amanda
 */
public interface PesertaDao extends PagingAndSortingRepository<Peserta, String>{
    
}